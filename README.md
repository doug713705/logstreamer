# logstreamer

LogStreamer est un logiciel permettant la diffusion sonore d'un fichier texte (mais pas que) via un serveur
de streaming Icecast.

La position dans la table des caractères de chaque caractère du fichier est convertie en une fréquence qui est
échantillonée en fonction des paramètres passés en option.

L'ensemble constituera un fichier WAV qui sera ensuite converti en OGG avant d'être envoyé vers un serveur
Icecast pour diffusion.


## Prérequis

* python3
* libshout
* python-shout (```pip3 install python-shout```)
* vorbis-tools

Les utilisateurs de distributions moisies basées sur Debian et/ou CentOS/RHEL (CentOS/Debian y compris) devront
au préalable installer les paquets suivants:
* python3-dev
* libshout3-dev


## Utilisation

```shell
logstreamer.py --help
usage: logstreamer.py [-h] --log LOG --host HOST --port PORT --user USER
                      --password PASSWORD --mount MOUNT [--workdir WORKDIR]
                      [--duration DURATION] [--amplitude AMPLITUDE]
                      [--framerate FRAMERATE] [--adjust ADJUST ADJUST]
                      [--exclude [EXCLUDE [EXCLUDE ...]]]
                      [--protocol PROTOCOL] [--ice_buffer ICE_BUFFER]
                      [--debug DEBUG]

optional arguments:
  -h, --help            show this help message and exit
  --log LOG             Le fichier de log à traiter (requis)
  --host HOST           Server Icecast2 (requis)
  --port PORT           Port TCP sur lequel contacter le serveur Icecast2
                        (requis)
  --user USER           Nom d'utilisateur (requis)
  --password PASSWORD   Mot de passe (requis)
  --mount MOUNT         Point de montage Icecast2 (requis)
  --workdir WORKDIR     Le répertoire de travail (défaut: /tmp)
  --duration DURATION   La durée d'une note (défaut: 0.008)
  --amplitude AMPLITUDE
                        Le niveau sonore (défaut: 0.5)
  --framerate FRAMERATE
                        Le taux d'échantillonage (défaut: 44100)
  --adjust ADJUST ADJUST
                        Facteurs d'ajustement (défaut: 5000 20000)
  --exclude [EXCLUDE [EXCLUDE ...]]
                        Liste des caractères non traités (défaut: [])
  --protocol PROTOCOL   Protocole à utiliser (défaut: http)
  --ice_buffer ICE_BUFFER
                        Taille du tampon Icecast (défaut: 32768)
  --debug DEBUG         Affiche l'activité (défaut: False)
```

## Exemple

```
logstreamer.py --log /var/log/messages \
               --host serviette.tetalab.org \
               --port 8000 \
               --user tetalab \
               --password XXXXXX \
               --mount /radiology.m4a
```


## Démo

[http://serviette.mixart-myrys.org:8000/radiology.m4a](http://serviette.mixart-myrys.org:8000/radiology.m4a)
